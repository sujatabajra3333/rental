from unicodedata import category
from django.db import models

# Create your models here.
class RentalProperty(models.Model):
    rentalcategory = models.CharField(max_length=100)

    def __str__(self):
        return self.rentalcategory

class RentalDetail(models.Model):
    rentalproperty = models.ForeignKey(RentalProperty, on_delete=models.CASCADE)
    tenant = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    price = models.IntegerField()
    description = models.TextField(max_length = 1000)
    created_added = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='uploads/', blank=True, null=True)

    def __str__(self):
        return f"{self.rentalproperty}:{self.decription}"
