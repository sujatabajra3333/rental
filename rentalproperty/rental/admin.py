from django.contrib import admin
from .models import RentalProperty,RentalDetail
# Register your models here.
admin.site.register(RentalProperty)
admin.site.register(RentalDetail)
