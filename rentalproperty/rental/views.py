from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .serializers import RentalPropertySerializers, RentalDetailSerializers
from .models import RentalProperty, RentalDetail
# Create your views here.
class RentalPropertyViewSet(ModelViewSet):
    queryset = RentalProperty.objects.all()
    serializer_class = RentalPropertySerializers

class RentalDetailViewSet(ModelViewSet):
    queryset = RentalDetail.objects.all()
    serializer_class = RentalDetailSerializers
