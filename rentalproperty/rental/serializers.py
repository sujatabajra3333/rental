from rest_framework import serializers
from .models import RentalProperty, RentalDetail
from rest_framework.serializers import ModelSerializer
class RentalPropertySerializers(ModelSerializer):
    class Meta:
        model = RentalProperty
        fields = ['rentalcategory']

class RentalDetailSerializers(ModelSerializer):
    class Meta: 
        model = RentalDetail
        fields = ['rentalproperty','location','tenant','description','price','image']
  